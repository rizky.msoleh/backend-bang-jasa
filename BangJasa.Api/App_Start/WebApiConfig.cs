﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace BangJasa.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Added config
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("image/jpeg"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("image/jpg"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("image/png"));
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // Web API routes
            config.MapHttpAttributeRoutes();

            #region Customer

            config.Routes.MapHttpRoute(
                name: "CustomerLogin",
                routeTemplate: "api/user/login",
                defaults: new { controller = "Customer", action = "Login" }
            );

            config.Routes.MapHttpRoute(
                name: "CustomerRegister",
                routeTemplate: "api/user/register",
                defaults: new { controller = "Customer", action = "Register" }
            );

            config.Routes.MapHttpRoute(
                name: "CustomerRetriveById",
                routeTemplate: "api/user/{id}",
                defaults: new { controller = "Customer", action = "RetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "CustomerUpdate",
                routeTemplate: "api/user/update",
                defaults: new { controller = "Customer", action = "Update" }
            );

            #endregion

            #region PENGAJUAN

            config.Routes.MapHttpRoute(
                name: "CreatePengajuan",
                routeTemplate: "api/pengajuan/create",
                defaults: new { controller = "Pengajuan", action = "Create" }
            );

            config.Routes.MapHttpRoute(
                name: "UpdatePengajuan",
                routeTemplate: "api/pengajuan/update",
                defaults: new { controller = "Pengajuan", action = "Update" }
            );

            config.Routes.MapHttpRoute(
                name: "PengajuanRetriveById",
                routeTemplate: "api/pengajuan/{id}",
                defaults: new { controller = "Pengajuan", action = "RetriveById" }
            );

            #endregion

            #region Pembayaran

            config.Routes.MapHttpRoute(
                name: "PembayaranRetriveById",
                routeTemplate: "api/pembayaran/{id}",
                defaults: new { controller = "Pembayaran", action = "RetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "PembyaranRetriveAll",
                routeTemplate: "api/pengajuan",
                defaults: new { controller = "Pengajuan", action = "Update" }
            );

            #endregion

            #region COMMON

            config.Routes.MapHttpRoute(
                name: "ProvinceRetriveAll",
                routeTemplate: "api/propinsi",
                defaults: new { controller = "CommonData", action = "ProvinceRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "ProvinsiRetriveById",
                routeTemplate: "api/propinsi/{id}",
                defaults: new { controller = "CommonData", action = "RetriveById" }
            );


            config.Routes.MapHttpRoute(
                name: "KabupatenRetriveAll",
                routeTemplate: "api/kabupaten",
                defaults: new { controller = "CommonData", action = "KabupatenRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "KabupatenRetriveById",
                routeTemplate: "api/kabupaten/{id}",
                defaults: new { controller = "CommonData", action = "KabupatenRetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "KotaRetriveAll",
                routeTemplate: "api/kota",
                defaults: new { controller = "CommonData", action = "KotaRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "KotaRetriveById",
                routeTemplate: "api/kota/{id}",
                defaults: new { controller = "CommonData", action = "KotaRetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "FaskesRetriveAll",
                routeTemplate: "api/faskes",
                defaults: new { controller = "CommonData", action = "FaskesRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "FaskesRetriveById",
                routeTemplate: "api/faskes/{id}",
                defaults: new { controller = "CommonData", action = "FaskesRetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "HubunganPelaporRetriveAll",
                routeTemplate: "api/hubunganpelapor",
                defaults: new { controller = "CommonData", action = "HubunganPelaporRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "HubunganPelaporRetriveById",
                routeTemplate: "api/hubunganpelapor/{id}",
                defaults: new { controller = "CommonData", action = "HubunganPelaporRetriveById" }
            );

            config.Routes.MapHttpRoute(
                name: "KondisiKorbanRetriveAll",
                routeTemplate: "api/kondisikorban",
                defaults: new { controller = "CommonData", action = "KondisiKorbanRetriveAll" }
            );

            config.Routes.MapHttpRoute(
                name: "KondisiKorbanRetriveById",
                routeTemplate: "api/kondisikorban/{id}",
                defaults: new { controller = "CommonData", action = "KondisiKorbanRetriveById" }
            );

            #endregion
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}