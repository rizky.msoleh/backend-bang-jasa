﻿using Resource.Business;
using Resource.Helper;
using Resource.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BangJasa.Api.Controllers
{
    public class CommonDataController : ApiController
    {
        private JsonEntity json = new JsonEntity();

        [HttpGet]
        public HttpResponseMessage ProvinceRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                ProvinsiVM searchModel = new ProvinsiVM();
                PaggingResult message = new ProvinsiBusiness().Pagging(searchModel, limit, offset);

                List<ProvinsiVM> results = message.Data as List<ProvinsiVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveProvinceById(long id)
        {
            try
            {
                ProvinsiVM results = new ProvinsiBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage KotaRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                KotaVM searchModel = new KotaVM();
                PaggingResult message = new KotaBusiness().Pagging(searchModel, limit, offset);

                List<KotaVM> results = message.Data as List<KotaVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveKotaById(long id)
        {
            try
            {
                KotaVM results = new KotaBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage KabupatenRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                KabupatenVM searchModel = new KabupatenVM();
                PaggingResult message = new KabupatenBusiness().Pagging(searchModel, limit, offset);

                List<KabupatenVM> results = message.Data as List<KabupatenVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveKabupatenById(long id)
        {
            try
            {
                KabupatenVM results = new KabupatenBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage FaskesRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                FaskesVM searchModel = new FaskesVM();
                PaggingResult message = new FaskesBusiness().Pagging(searchModel, limit, offset);

                List<FaskesVM> results = message.Data as List<FaskesVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveFaskesById(long id)
        {
            try
            {
                FaskesVM results = new FaskesBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage HubunganPelaporRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                HubunganPelaporVM searchModel = new HubunganPelaporVM();
                PaggingResult message = new HubunganPelaporBusiness().Pagging(searchModel, limit, offset);

                List<HubunganPelaporVM> results = message.Data as List<HubunganPelaporVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveHubunganPelaporById(long id)
        {
            try
            {
                HubunganPelaporVM results = new HubunganPelaporBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage KondisiKorbanRetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                KondisiKorbanVM searchModel = new KondisiKorbanVM();
                PaggingResult message = new KondisiKorbanBusiness().Pagging(searchModel, limit, offset);

                List<KondisiKorbanVM> results = message.Data as List<KondisiKorbanVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    if (message.Status == 0)
                    {
                        json.error = true;
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Data.ToString());
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                    else
                    {
                        json.AddData(null);
                        json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data not Found");
                        return JsonHelper.JsonResponse(json, this.Request);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        //[AjpandAuth]
        [HttpGet]
        public HttpResponseMessage RetriveKondisiKorbanById(long id)
        {
            try
            {
                KondisiKorbanVM results = new KondisiKorbanBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "Data Not Found");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), "Something wrong, please contact your admin");
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }
    }
}