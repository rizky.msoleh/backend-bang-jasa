﻿using Resource.Business;
using Resource.Helper;
using Resource.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BangJasa.Api.Controllers
{
    public class PembayaranController : ApiController
    {
        private JsonEntity json = new JsonEntity();

        [HttpGet]
        public HttpResponseMessage RetriveById(long id)
        {
            try
            {
                PembayaranVM results = new PembayaranBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "NOT FOUND");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage RetriveAll([FromUri]ListVM dto)
        {
            try
            {
                int limit = dto.limit != null && dto.limit != 0 ? dto.limit.Value : 10;
                int offset = dto.offset != null && dto.offset != 0 ? dto.offset.Value : 1;
                int sortCode = dto.sort_code != null ? dto.sort_code.Value : 2;

                PembayaranVM searchModel = new PembayaranVM();

                PaggingResult message = new PembayaranBusiness().Paging(searchModel, limit, offset, sortCode);
                List<PembayaranVM> results = message.Data as List<PembayaranVM>;
                if (results != null && results.Count > 0)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "SUCCESS");
                    json.AddTotalData(results.Count);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "NOT FOUND");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }
    }
}