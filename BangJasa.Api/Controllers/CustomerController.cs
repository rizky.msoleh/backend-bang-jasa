﻿using Resource.Business;
using Resource.Helper;
using Resource.Models;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace BangJasa.Api.Controllers
{
    public class CustomerController : ApiController
    {
        private JsonEntity json = new JsonEntity();

        [HttpPost]
        public HttpResponseMessage Login(CustomerLoginVM model)
        {
            try
            {
                model.password = model.password.TrimEnd();
                if (!ModelState.IsValid)
                {
                    String pesan = string.Join("<br/>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), pesan);
                    return JsonHelper.JsonResponse(json, this.Request);
                }

                AlertMessage result = new AlertMessage();
                CustomerBusiness modelBussiness = new CustomerBusiness();
                result = modelBussiness.Login(model);
                if (result.Status == 1)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), result.Text);
                    json.AddTotalData(1);
                    json.AddData(result.Data);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), result.Text);
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpPost]
        public HttpResponseMessage Register(RegisterVM model)
        {
            try
            {
                model.password = model.password.TrimEnd();
                if (!ModelState.IsValid)
                {
                    String pesan = string.Join("<br/>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), pesan);
                    return JsonHelper.JsonResponse(json, this.Request);
                }

                AlertMessage result = new AlertMessage();
                CustomerBusiness modelBussiness = new CustomerBusiness();
                result = modelBussiness.Register(model);
                if (result.Status == 1)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), result.Text);
                    json.AddTotalData(1);
                    json.AddData(result.Data);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), result.Text);
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpPost]
        public HttpResponseMessage Update()
        {
            try
            {
                NameValueCollection ModelVarCollection = HttpContext.Current.Request.Form;
                ModelState.Clear();
                CustomerUpdateVM model = new CustomerUpdateVM();
                //Mapping Collection To Model
                foreach (string kvp in ModelVarCollection.AllKeys)
                {
                    PropertyInfo pi = model.GetType().GetProperty(kvp, BindingFlags.Public | BindingFlags.Instance);
                    if (pi != null && !string.IsNullOrEmpty(ModelVarCollection[kvp].ToString()))
                    {
                        if (pi.PropertyType.FullName == typeof(Nullable<long>).FullName)
                        {
                            pi.SetValue(model, Convert.ToInt64(ModelVarCollection[kvp]), null);
                        }
                        else if (pi.PropertyType.FullName == typeof(long).FullName)
                        {
                            pi.SetValue(model, Convert.ToInt64(ModelVarCollection[kvp]), null);
                        }
                        else if (pi.PropertyType.FullName == typeof(Nullable<int>).FullName)
                        {
                            pi.SetValue(model, Convert.ToInt32(ModelVarCollection[kvp]), null);
                        }
                        else if (pi.PropertyType.FullName == typeof(int).FullName)
                        {
                            pi.SetValue(model, Convert.ToInt32(ModelVarCollection[kvp]), null);
                        }
                        else if (pi.PropertyType.FullName == typeof(Nullable<bool>).FullName)
                        {
                            pi.SetValue(model, Convert.ToBoolean(ModelVarCollection[kvp]), null);
                        }
                        else if (pi.PropertyType.FullName == typeof(bool).FullName)
                        {
                            pi.SetValue(model, Convert.ToBoolean(ModelVarCollection[kvp]), null);
                        }
                        else
                        {
                            pi.SetValue(model, ModelVarCollection[kvp], null);
                        }
                    }
                }

                AlertMessage message = new AlertMessage();
                CustomerBusiness modelBussiness = new CustomerBusiness();
                message = modelBussiness.Update(model);
                if (message.Status == 1)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), message.Text.Trim());
                    json.AddTotalData(0);
                    json.AddData(null);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), message.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }

        [HttpGet]
        public HttpResponseMessage RetriveById(long id)
        {
            try
            {
                CustomerVM results = new CustomerBusiness().FindByPK(id);
                if (results != null)
                {
                    json.AddAlert(((int)HttpStatusCode.OK).ToString(), "Success");
                    json.AddTotalData(1);
                    json.AddData(results);
                }
                else
                {
                    json.AddData(null);
                    json.AddAlert(((int)HttpStatusCode.NotFound).ToString(), "NOT FOUND");
                    return JsonHelper.JsonResponse(json, this.Request);
                }
            }
            catch (Exception ex)
            {
                //LoggingHelper.DebugLogging((string)Request.GetRequestContext().RouteData.Values["action"], ex);
                json.SetError(true);
                json.AddAlert(((int)HttpStatusCode.BadRequest).ToString(), ex.Message);
            }

            return JsonHelper.JsonResponse(json, this.Request);
        }
    }
}