﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class KondisiKorbanBusiness
    {
        public List<KondisiKorban> Search(int Limit, int Offset)
        {
            List<KondisiKorbanVM> listModelView = new List<KondisiKorbanVM>();

            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();

            List<KondisiKorban> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (KondisiKorban varModelDb in message)
            {
                listModelView.Add(new KondisiKorbanVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(KondisiKorbanVM modelView, int Limit, int Offset)
        {
            List<KondisiKorbanVM> listModelView = new List<KondisiKorbanVM>();

            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();

            var condition = PredicateBuilder.True<KondisiKorban>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (KondisiKorban varModelDb in message.Data as List<KondisiKorban>)
            {
                listModelView.Add(new KondisiKorbanVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<KondisiKorban> Repo = new Repository<KondisiKorban>();
            KondisiKorban cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public KondisiKorbanVM FindByPK(long primaryKeyModel)
        {
            KondisiKorban varModelDB = new KondisiKorban();

            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();
            KondisiKorban modelDB = modelRepo.FindByPk(primaryKeyModel);
            KondisiKorbanVM modelView = new KondisiKorbanVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(KondisiKorbanVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();
            KondisiKorban modelDB = modelRepo.FindByPk(modelView.IdKondisiKorban);

            KondisiKorban modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public KondisiKorban SetModelViewToModelDB(KondisiKorban modelDB, KondisiKorbanVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(KondisiKorbanVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            KondisiKorban modelDB = new KondisiKorban();

            KondisiKorban modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<KondisiKorban> modelRepo = new Repository<KondisiKorban>();
            KondisiKorban modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}