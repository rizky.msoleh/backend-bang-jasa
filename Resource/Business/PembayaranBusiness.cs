﻿using System.Collections.Generic;
using System.Linq;
using Resource.Helper;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;

namespace Resource.Business
{
    public class PembayaranBusiness
    {
        public PaggingResult Paging(PembayaranVM modelView, int Limit, int Offset, long loggedUserId)
        {
            List<PembayaranVM> listModelView = new List<PembayaranVM>();

            Repository<Pembayaran> modelRepo = new Repository<Pembayaran>();

            var condition = PredicateBuilder.True<Pembayaran>();

            if (!string.IsNullOrEmpty(modelView.NomorPembayaran))
            {
                condition = condition.And(x => x.NomorPembayaran.Trim().ToLower().Contains(modelView.NomorPembayaran.Trim().ToLower()));
            }

            PaggingResult message = modelRepo.Paging(condition, "NomorPembayaran", Limit, Offset, false);

            foreach (Pembayaran varModelDb in message.Data as List<Pembayaran>)
            {
                listModelView.Add(new PembayaranVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public PembayaranVM FindByPK(long primaryKeyModel)
        {
            Repository<Pembayaran> modelRepo = new Repository<Pembayaran>();
            Pembayaran modelDB = modelRepo.FindByPk(primaryKeyModel);
            PembayaranVM modelView = new PembayaranVM(modelDB);

            return modelView;
        }
    }
}