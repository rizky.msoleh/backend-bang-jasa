﻿using Newtonsoft.Json;
using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Linq;

namespace Resource.Business
{
    public class CustomerBusiness
    {
        public AlertMessage Login(CustomerLoginVM model)
        {
            AlertMessage message = new AlertMessage();
            DateTime date = DateTime.UtcNow;

            try
            {
                var condition = PredicateBuilder.True<Customer>();

                condition = condition.And(x => x.Username.Trim().Equals(model.username.Trim()));
                condition = condition.And(x => x.Password.Trim().Equals(model.password.Trim()));

                Repository<Customer> modelRepo = new Repository<Customer>();
                Customer modelDB = modelRepo.Find(condition, null, 0, 0).FirstOrDefault();

                if (modelDB != null)
                {
                    message.Status = 1;
                    message.Text = string.Format(APIMessage.LOGIN_SUCCESS, modelDB.Fullname);
                    message.Data = JsonConvert.SerializeObject(modelDB);

                    modelDB.Token = EncryptionHelper.GetGUID();
                    modelDB.LastLogin = date;
                    modelRepo.Update(modelDB);
                }
                else
                {
                    message.Status = 0;
                    message.Data = null;
                    message.Text = string.Format(APIMessage.LOGIN_FAILED);
                }
            }
            catch (Exception exc)
            {
                message.Status = 0;
                message.Data = null;
                message.Text = string.Format(APIMessage.ERROR, exc.Message);
            }

            return message;
        }

        private Customer SetRegisterModelViewToModelDB(Customer modelDB, RegisterVM modelView)
        {
            modelDB.Username = modelView.username;
            modelDB.Password = modelView.password;
            modelDB.Fullname = modelView.fullname;
            modelDB.Email = modelView.email;

            return modelDB;
        }

        public AlertMessage Register(RegisterVM model)
        {
            AlertMessage message = new AlertMessage();
            DateTime date = DateTime.UtcNow;
            Repository<Customer> Repo = new Repository<Customer>();
            Customer cekMod = Repo.Find(x => x.Email.Trim().Equals(model.email.Trim()) || x.Username.Trim().Equals(model.username.Trim()) || x.Handphone.Trim().Equals(model.handphone.Trim())).FirstOrDefault();

            if (cekMod == null)
            {//Jika Data Belum Ada di DB
                Customer modelDB = new Customer();

                Customer modelDBToSave = SetRegisterModelViewToModelDB(modelDB, model);
                Repository<Customer> modelRepo = new Repository<Customer>();
                modelDBToSave.IsDeleted = false;
                modelDBToSave.CreatedBy = modelDBToSave.CustomerId;
                modelDBToSave.CreatedDate = date;
                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, model.fullname);
                }
                else
                {
                    message.Status = 0;
                    message.Text = String.Format(APIMessage.SAVE_FAILED, model.fullname);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format("Username, Email atau nomor handphone sudah terdaftar");
            }

            return message;
        }

        public CustomerVM FindByPK(long primaryKeyModel)
        {
            Repository<Customer> modelRepo = new Repository<Customer>();
            Customer modelDB = modelRepo.FindByPk(primaryKeyModel);
            CustomerVM modelView = new CustomerVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(CustomerUpdateVM model)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Customer> modelRepo = new Repository<Customer>();
            Customer modToSave = modelRepo.FindByPk(model.CustomerId);

            modToSave = SetUpdateModelViewToModelDB(modToSave, model);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Fullname);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Fullname);
            }

            return message;
        }

        public Customer SetUpdateModelViewToModelDB(Customer modelDB, CustomerUpdateVM modelView)
        {
            modelDB.Username = modelView.username;

            modelDB.Fullname = modelView.fullname;
            modelDB.Handphone = modelView.handphone;
            modelDB.Email = modelView.email;
            if (modelView.photo != null)
            {
                FileHelper.DeleteFile(modelDB.Foto);
                modelDB.Foto = FileHelper.UploadFileAPI("Photo_", modelDB.Username, modelView.photo);
            }

            return modelDB;
        }
    }
}