﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;

namespace Resource.Business
{
    public class ProvinsiBusiness
    {
        public List<Provinsi> Search(int Limit, int Offset)
        {
            List<ProvinsiVM> listModelView = new List<ProvinsiVM>();

            Repository<Provinsi> modelRepo = new Repository<Provinsi>();

            List<Provinsi> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (Provinsi varModelDb in message)
            {
                listModelView.Add(new ProvinsiVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(ProvinsiVM modelView, int Limit, int Offset)
        {
            List<ProvinsiVM> listModelView = new List<ProvinsiVM>();

            Repository<Provinsi> modelRepo = new Repository<Provinsi>();

            var condition = PredicateBuilder.True<Provinsi>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }



            condition = condition.And(x => x.Dihapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (Provinsi varModelDb in message.Data as List<Provinsi>)
            {
                listModelView.Add(new ProvinsiVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<Provinsi> Repo = new Repository<Provinsi>();
            Provinsi cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public ProvinsiVM FindByPK(long primaryKeyModel)
        {

            Provinsi varModelDB = new Provinsi();

            Repository<Provinsi> modelRepo = new Repository<Provinsi>();
            Provinsi modelDB = modelRepo.FindByPk(primaryKeyModel);
            ProvinsiVM modelView = new ProvinsiVM(modelDB);

            return modelView;
        }



        public AlertMessage Update(ProvinsiVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Provinsi> modelRepo = new Repository<Provinsi>();
            Provinsi modelDB = modelRepo.FindByPk(modelView.IdProvinsi);

            Provinsi modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public Provinsi SetModelViewToModelDB(Provinsi modelDB, ProvinsiVM modelView)
        {

            modelDB.Nama = modelView.Nama;
            
            return modelDB;
        }

        public AlertMessage Add(ProvinsiVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Provinsi modelDB = new Provinsi();

            Provinsi modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<Provinsi> modelRepo = new Repository<Provinsi>();
            modelDBToSave.Dihapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);

            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Provinsi> modelRepo = new Repository<Provinsi>();
            Provinsi modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Dihapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}