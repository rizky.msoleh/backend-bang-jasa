﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class KabupatenBusiness
    {
        public List<Kabupaten> Search(int Limit, int Offset)
        {
            List<KabupatenVM> listModelView = new List<KabupatenVM>();

            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();

            List<Kabupaten> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (Kabupaten varModelDb in message)
            {
                listModelView.Add(new KabupatenVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(KabupatenVM modelView, int Limit, int Offset)
        {
            List<KabupatenVM> listModelView = new List<KabupatenVM>();

            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();

            var condition = PredicateBuilder.True<Kabupaten>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (Kabupaten varModelDb in message.Data as List<Kabupaten>)
            {
                listModelView.Add(new KabupatenVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<Kabupaten> Repo = new Repository<Kabupaten>();
            Kabupaten cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public KabupatenVM FindByPK(long primaryKeyModel)
        {
            Kabupaten varModelDB = new Kabupaten();

            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();
            Kabupaten modelDB = modelRepo.FindByPk(primaryKeyModel);
            KabupatenVM modelView = new KabupatenVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(KabupatenVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();
            Kabupaten modelDB = modelRepo.FindByPk(modelView.IdKabupaten);

            Kabupaten modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public Kabupaten SetModelViewToModelDB(Kabupaten modelDB, KabupatenVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(KabupatenVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Kabupaten modelDB = new Kabupaten();

            Kabupaten modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Kabupaten> modelRepo = new Repository<Kabupaten>();
            Kabupaten modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}