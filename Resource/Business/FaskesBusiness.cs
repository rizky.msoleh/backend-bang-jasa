﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class FaskesBusiness
    {
        public List<Faske> Search(int Limit, int Offset)
        {
            List<FaskesVM> listModelView = new List<FaskesVM>();

            Repository<Faske> modelRepo = new Repository<Faske>();

            List<Faske> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (Faske varModelDb in message)
            {
                listModelView.Add(new FaskesVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(FaskesVM modelView, int Limit, int Offset)
        {
            List<FaskesVM> listModelView = new List<FaskesVM>();

            Repository<Faske> modelRepo = new Repository<Faske>();

            var condition = PredicateBuilder.True<Faske>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (Faske varModelDb in message.Data as List<Faske>)
            {
                listModelView.Add(new FaskesVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<Faske> Repo = new Repository<Faske>();
            Faske cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public FaskesVM FindByPK(long primaryKeyModel)
        {
            Faske varModelDB = new Faske();

            Repository<Faske> modelRepo = new Repository<Faske>();
            Faske modelDB = modelRepo.FindByPk(primaryKeyModel);
            FaskesVM modelView = new FaskesVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(FaskesVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Faske> modelRepo = new Repository<Faske>();
            Faske modelDB = modelRepo.FindByPk(modelView.IdFaskes);

            Faske modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public Faske SetModelViewToModelDB(Faske modelDB, FaskesVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(FaskesVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Faske modelDB = new Faske();

            Faske modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<Faske> modelRepo = new Repository<Faske>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Faske> modelRepo = new Repository<Faske>();
            Faske modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}