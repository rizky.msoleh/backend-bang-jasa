﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class KasusKecelakaanBusiness
    {
        public List<KasusKecelakaan> Search(int Limit, int Offset)
        {
            List<KasusKecelakaanVM> listModelView = new List<KasusKecelakaanVM>();

            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();

            List<KasusKecelakaan> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (KasusKecelakaan varModelDb in message)
            {
                listModelView.Add(new KasusKecelakaanVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(KasusKecelakaanVM modelView, int Limit, int Offset)
        {
            List<KasusKecelakaanVM> listModelView = new List<KasusKecelakaanVM>();

            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();

            var condition = PredicateBuilder.True<KasusKecelakaan>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (KasusKecelakaan varModelDb in message.Data as List<KasusKecelakaan>)
            {
                listModelView.Add(new KasusKecelakaanVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<KasusKecelakaan> Repo = new Repository<KasusKecelakaan>();
            KasusKecelakaan cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public KasusKecelakaanVM FindByPK(long primaryKeyModel)
        {
            KasusKecelakaan varModelDB = new KasusKecelakaan();

            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();
            KasusKecelakaan modelDB = modelRepo.FindByPk(primaryKeyModel);
            KasusKecelakaanVM modelView = new KasusKecelakaanVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(KasusKecelakaanVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();
            KasusKecelakaan modelDB = modelRepo.FindByPk(modelView.IdKasusKecelakaan);

            KasusKecelakaan modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public KasusKecelakaan SetModelViewToModelDB(KasusKecelakaan modelDB, KasusKecelakaanVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(KasusKecelakaanVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            KasusKecelakaan modelDB = new KasusKecelakaan();

            KasusKecelakaan modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<KasusKecelakaan> modelRepo = new Repository<KasusKecelakaan>();
            KasusKecelakaan modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}