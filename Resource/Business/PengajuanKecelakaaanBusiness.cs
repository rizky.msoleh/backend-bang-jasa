﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class PengajuanKecelakaanBusiness
    {
        public List<PengajuanKecelakaan> Search(int Limit, int Offset)
        {
            List<PengajuanKecelakaanVM> listModelView = new List<PengajuanKecelakaanVM>();

            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();

            List<PengajuanKecelakaan> message = modelRepo.Find(null, "NomorPelaporan", Limit, Offset);

            foreach (PengajuanKecelakaan varModelDb in message)
            {
                listModelView.Add(new PengajuanKecelakaanVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(PengajuanKecelakaanVM modelView, int Limit, int Offset)
        {
            List<PengajuanKecelakaanVM> listModelView = new List<PengajuanKecelakaanVM>();

            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();

            var condition = PredicateBuilder.True<PengajuanKecelakaan>();

            if (!string.IsNullOrEmpty(modelView.NomorPelaporan))
            {
                condition = condition.And(x => x.NomorPelaporan.Trim().ToLower().Contains(modelView.NomorPelaporan.Trim().ToLower()));
            }

            //condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "NomorPelaporan", Limit, Offset, false);

            foreach (PengajuanKecelakaan varModelDb in message.Data as List<PengajuanKecelakaan>)
            {
                listModelView.Add(new PengajuanKecelakaanVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<PengajuanKecelakaan> Repo = new Repository<PengajuanKecelakaan>();
            PengajuanKecelakaan cekMod = Repo.Find(x => x.NomorPelaporan == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public PengajuanKecelakaanVM FindByPK(long primaryKeyModel)
        {
            PengajuanKecelakaan varModelDB = new PengajuanKecelakaan();

            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();
            PengajuanKecelakaan modelDB = modelRepo.FindByPk(primaryKeyModel);
            PengajuanKecelakaanVM modelView = new PengajuanKecelakaanVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(PengajuanKecelakaanUpdateVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();
            PengajuanKecelakaan modToSave = modelRepo.FindByPk(modelView.IdPengajuan);

            modToSave = SetUpdateModelViewToModelDB(modToSave, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.NomorPelaporan);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.NomorPelaporan);
            }

            return message;
        }

        public PengajuanKecelakaan SetUpdateModelViewToModelDB(PengajuanKecelakaan modelDB, PengajuanKecelakaanUpdateVM modelView)
        {
            modelDB.NIK = modelView.NIK;
            modelDB.Nama = modelView.Nama;
            modelDB.TanggalKecelakaan = modelView.TanggalKecelakaan.StringToDateTime();
            modelDB.IdProvinsi = modelView.IdProvinsi;
            modelDB.IdKabupaten = modelView.IdKabupaten;
            modelDB.IdKota = modelView.IdKota;
            modelDB.Umur = modelView.Umur;
            modelDB.JenisKelamin = modelView.JenisKelamin;
            modelDB.Alamat = modelView.Alamat;
            modelDB.IdKondisiKorban = modelView.IdKondisiKorban;
            modelDB.IdKasusKecelakaan = modelView.IdKasusKecelakaan;
            modelDB.IdHubunganPelapor = modelView.IdHubunganPelapor;
            modelDB.NamaPelapor = modelView.NamaPelapor;
            modelDB.HandphonePelapor = modelView.HandphonePelapor;
            modelDB.IdProvinsiPelapor = modelView.IdProvinsiPelapor;
            modelDB.IdKotaPelapor = modelView.IdKotaPelapor;
            modelDB.IdFaskes = modelView.IdFaskes;
            modelDB.LaporanPolisi = modelView.LaporanPolisi;
            modelDB.NoLaporan = modelView.NoLaporan;

            if (modelView.KTP != null)
            {
                FileHelper.DeleteFile(modelDB.KTP);
                modelDB.KTP = FileHelper.UploadFileAPI("KTP_", modelDB.NomorPelaporan, modelView.KTP);
            }
            else
            {
                FileHelper.DeleteFile(modelDB.KTP);
                modelDB.KTP = null;
            }

            if (modelView.KwitansiRS != null)
            {
                FileHelper.DeleteFile(modelDB.KwitansiRS);
                modelDB.KwitansiRS = FileHelper.UploadFileAPI("KwitansiRS_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }
            else
            {
                FileHelper.DeleteFile(modelDB.KwitansiRS);
                modelDB.KwitansiRS = null;
            }

            if (modelView.SuratKeteranganKesehatan != null)
            {
                FileHelper.DeleteFile(modelDB.SuratKeteranganKesehatan);
                modelDB.SuratKeteranganKesehatan = FileHelper.UploadFileAPI("SuratKeteranganKesehatan_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }
            else
            {
                FileHelper.DeleteFile(modelDB.SuratKeteranganKesehatan);
                modelDB.SuratKeteranganKesehatan = null;
            }

            if (modelView.SuratKeteranganKesehatan != null)
            {
                FileHelper.DeleteFile(modelDB.SuratKeteranganAhliWaris);
                modelDB.SuratKeteranganAhliWaris = FileHelper.UploadFileAPI("SuratKeteranganAhliWaris_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }
            else
            {
                FileHelper.DeleteFile(modelDB.SuratKeteranganAhliWaris);
                modelDB.SuratKeteranganAhliWaris = null;
            }

            if (modelView.SuratLaporanPolisi != null)
            {
                FileHelper.DeleteFile(modelDB.SuratLaporanPolisi);
                modelDB.SuratLaporanPolisi = FileHelper.UploadFileAPI("SuratLaporanPolisi_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }
            else
            {
                FileHelper.DeleteFile(modelDB.SuratLaporanPolisi);
                modelDB.SuratLaporanPolisi = null;
            }
            
            return modelDB;
        }

        public PengajuanKecelakaan SetCreateModelViewToModelDB(PengajuanKecelakaan modelDB, PengajuanKecelakaanCreateVM modelView)
        {
            modelDB.NIK = modelView.NIK;
            modelDB.Nama = modelView.Nama;
            modelDB.TanggalKecelakaan = modelView.TanggalKecelakaan.StringToDateTime();
            modelDB.IdProvinsi = modelView.IdProvinsi;
            modelDB.IdKabupaten = modelView.IdKabupaten;
            modelDB.IdKota = modelView.IdKota;
            modelDB.Umur = modelView.Umur;
            modelDB.JenisKelamin = modelView.JenisKelamin;
            modelDB.Alamat = modelView.Alamat;
            modelDB.IdKondisiKorban = modelView.IdKondisiKorban;
            modelDB.IdKasusKecelakaan = modelView.IdKasusKecelakaan;
            modelDB.IdHubunganPelapor = modelView.IdHubunganPelapor;
            modelDB.NamaPelapor = modelView.NamaPelapor;
            modelDB.HandphonePelapor = modelView.HandphonePelapor;
            modelDB.IdProvinsiPelapor = modelView.IdProvinsiPelapor;
            modelDB.IdKotaPelapor = modelView.IdKotaPelapor;
            modelDB.IdFaskes = modelView.IdFaskes;
            modelDB.LaporanPolisi = modelView.LaporanPolisi;
            modelDB.NoLaporan = modelView.NoLaporan;

            if (modelView.KTP != null)
            {
                modelDB.KTP = FileHelper.UploadFileAPI("KTP_", modelDB.NomorPelaporan, modelView.KTP);
            }

            if (modelView.KwitansiRS != null)
            {
                modelDB.KwitansiRS = FileHelper.UploadFileAPI("KwitansiRS_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }

            if (modelView.SuratKeteranganKesehatan != null)
            {
                modelDB.SuratKeteranganKesehatan = FileHelper.UploadFileAPI("SuratKeteranganKesehatan_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }

            if (modelView.SuratKeteranganKesehatan != null)
            {
                modelDB.SuratKeteranganAhliWaris = FileHelper.UploadFileAPI("SuratKeteranganAhliWaris_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }

            if (modelView.SuratLaporanPolisi != null)
            {
                modelDB.SuratLaporanPolisi = FileHelper.UploadFileAPI("SuratLaporanPolisi_", modelDB.NomorPelaporan, modelView.KwitansiRS);
            }

            return modelDB;
        }

        public AlertMessage Create(PengajuanKecelakaanCreateVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            PengajuanKecelakaan modelDBToSave = new PengajuanKecelakaan();
            string nomorPelaporan = EncryptionHelper.GenerateNomorPelaporan();
            modelDBToSave.NomorPelaporan = nomorPelaporan;

            modelDBToSave = SetCreateModelViewToModelDB(modelDBToSave, modelView);
            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();

            DateTime date = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Insert(modelDBToSave);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.SAVE_SUCCESS, nomorPelaporan);
            }
            else
            {
                message.Text = String.Format(APIMessage.SAVE_FAILED, resp.Message);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<PengajuanKecelakaan> modelRepo = new Repository<PengajuanKecelakaan>();
            PengajuanKecelakaan modelDB = modelRepo.FindByPk(PrimaryKey);

            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.NomorPelaporan);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.NomorPelaporan);
            }

            return message;
        }
    }
}