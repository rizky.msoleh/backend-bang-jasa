﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class HubunganPelaporBusiness
    {
        public List<HubunganPelapor> Search(int Limit, int Offset)
        {
            List<HubunganPelaporVM> listModelView = new List<HubunganPelaporVM>();

            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();

            List<HubunganPelapor> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (HubunganPelapor varModelDb in message)
            {
                listModelView.Add(new HubunganPelaporVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(HubunganPelaporVM modelView, int Limit, int Offset)
        {
            List<HubunganPelaporVM> listModelView = new List<HubunganPelaporVM>();

            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();

            var condition = PredicateBuilder.True<HubunganPelapor>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (HubunganPelapor varModelDb in message.Data as List<HubunganPelapor>)
            {
                listModelView.Add(new HubunganPelaporVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<HubunganPelapor> Repo = new Repository<HubunganPelapor>();
            HubunganPelapor cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public HubunganPelaporVM FindByPK(long primaryKeyModel)
        {
            HubunganPelapor varModelDB = new HubunganPelapor();

            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();
            HubunganPelapor modelDB = modelRepo.FindByPk(primaryKeyModel);
            HubunganPelaporVM modelView = new HubunganPelaporVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(HubunganPelaporVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();
            HubunganPelapor modelDB = modelRepo.FindByPk(modelView.IdHubunganPelapor);

            HubunganPelapor modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public HubunganPelapor SetModelViewToModelDB(HubunganPelapor modelDB, HubunganPelaporVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(HubunganPelaporVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            HubunganPelapor modelDB = new HubunganPelapor();

            HubunganPelapor modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<HubunganPelapor> modelRepo = new Repository<HubunganPelapor>();
            HubunganPelapor modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}