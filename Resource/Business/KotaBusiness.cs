﻿using Resource.Helper;
using Resource.Localization;
using Resource.Models;
using Resource.Models.DatabaseModel;
using Resource.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Resource.Business
{
    public class KotaBusiness
    {
        public List<Kota> Search(int Limit, int Offset)
        {
            List<KotaVM> listModelView = new List<KotaVM>();

            Repository<Kota> modelRepo = new Repository<Kota>();

            List<Kota> message = modelRepo.Find(null, "Nama", Limit, Offset);

            foreach (Kota varModelDb in message)
            {
                listModelView.Add(new KotaVM(varModelDb));
            }

            return message;
        }

        public PaggingResult Pagging(KotaVM modelView, int Limit, int Offset)
        {
            List<KotaVM> listModelView = new List<KotaVM>();

            Repository<Kota> modelRepo = new Repository<Kota>();

            var condition = PredicateBuilder.True<Kota>();

            if (!string.IsNullOrEmpty(modelView.Nama))
            {
                condition = condition.And(x => x.Nama.Trim().ToLower().Contains(modelView.Nama.Trim().ToLower()));
            }

            condition = condition.And(x => x.Terhapus == false);

            PaggingResult message = modelRepo.Paging(condition, "Nama", Limit, Offset, false);

            foreach (Kota varModelDb in message.Data as List<Kota>)
            {
                listModelView.Add(new KotaVM(varModelDb));
            }

            message.Data = listModelView.ToList();
            return message;
        }

        public bool IsModelExists(string Name)
        {
            bool result = false;

            Repository<Kota> Repo = new Repository<Kota>();
            Kota cekMod = Repo.Find(x => x.Nama == Name).FirstOrDefault();

            if (cekMod != null)
            {
                result = true;
            }
            return result;
        }

        public KotaVM FindByPK(long primaryKeyModel)
        {
            Kota varModelDB = new Kota();

            Repository<Kota> modelRepo = new Repository<Kota>();
            Kota modelDB = modelRepo.FindByPk(primaryKeyModel);
            KotaVM modelView = new KotaVM(modelDB);

            return modelView;
        }

        public AlertMessage Update(KotaVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Kota> modelRepo = new Repository<Kota>();
            Kota modelDB = modelRepo.FindByPk(modelView.IdKota);

            Kota modToSave = SetModelViewToModelDB(modelDB, modelView);
            //modToSave.UpdatedBy = _LoggedUser.Id;
            //modToSave.UpdatedDate = DateTime.UtcNow;

            RepoResponse resp = modelRepo.Update(modToSave);

            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.UPDATE_SUCCESS, modToSave.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.UPDATE_FAILED, modToSave.Nama);
            }

            return message;
        }

        public Kota SetModelViewToModelDB(Kota modelDB, KotaVM modelView)
        {
            modelDB.Nama = modelView.Nama;

            return modelDB;
        }

        public AlertMessage Add(KotaVM modelView)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Kota modelDB = new Kota();

            Kota modelDBToSave = SetModelViewToModelDB(modelDB, modelView);
            Repository<Kota> modelRepo = new Repository<Kota>();
            modelDBToSave.Terhapus = false;
            if (!IsModelExists(modelView.Nama))
            {
                DateTime date = DateTime.UtcNow;
                //modelDBToSave.CreatedBy = _LoggedUser.Id;
                //modelDBToSave.CreatedDate = date;

                RepoResponse resp = modelRepo.Insert(modelDBToSave);
                if (resp.IsSuccess)
                {
                    message.Status = 1;
                    message.Text = String.Format(APIMessage.SAVE_SUCCESS, modelView.Nama);
                }
                else
                {
                    message.Text = String.Format(APIMessage.SAVE_FAILED, modelView.Nama);
                }
            }
            else
            {
                message.Status = 0;
                message.Text = String.Format(APIMessage.ERR_OBJECT_EXISTS, modelView.Nama);
            }

            return message;
        }

        public AlertMessage Delete(long PrimaryKey)
        {
            AlertMessage message = new AlertMessage() { Status = 0 };

            Repository<Kota> modelRepo = new Repository<Kota>();
            Kota modelDB = modelRepo.FindByPk(PrimaryKey);
            modelDB.Terhapus = true;
            //modelDB.UpdatedBy = _LoggedUser.Id;
            //modelDB.UpdatedDate = DateTime.UtcNow;
            RepoResponse resp = modelRepo.Update(modelDB);
            if (resp.IsSuccess)
            {
                message.Status = 1;
                message.Text = String.Format(APIMessage.DELETE_SUCCESS, modelDB.Nama);
            }
            else
            {
                message.Text = String.Format(APIMessage.DELETE_FAILED, modelDB.Nama);
            }

            return message;
        }
    }
}