﻿using Resource.Helper;
using Resource.Models.DatabaseModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Resource.Repository
{
    public class Repository<TEntity> where TEntity : class
    {
        public Expression<Func<TEntity, bool>> Condition { get; set; }
        public string OrderColumn { get; set; }
        public bool IsDesc { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public string ThenColumn { get; set; }
        public bool IsThenDesc { get; set; }

        public BangjasaEntities DbContext;

        //protected DbSet<TEntity> _DbSet;

        private string errorMessage = string.Empty;

        public Repository()
        {
            DbContext = GetDbContext();

            ResetQueryParams();
        }

        public void ResetQueryParams()
        {
            Condition = null;
            IsDesc = true;
            Limit = 1;
            Offset = 1;
            ThenColumn = null;
            IsThenDesc = true;
        }

        public BangjasaEntities GetDbContext()
        {
            return new BangjasaEntities();
        }

        private int? _GetPrimaryKeyValue(DbContext context, TEntity entity)
        {
            var objectStateEntry = ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager.GetObjectStateEntry(context.Entry(entity).Entity);
            object obj = objectStateEntry.EntityKey.EntityKeyValues[0].Value;

            return (obj == null ? (int?)null : Convert.ToInt16(obj));
        }

        public RepoResponse Insert(TEntity entity)
        {
            RepoResponse dbResponse = new RepoResponse() { IsSuccess = false };

            try
            {
                if (entity == null)
                {
                    dbResponse.Message = "NULL";
                    return dbResponse;
                }

                DbContext.Set<TEntity>().Add(entity);

                DbContext.SaveChanges();

                dbResponse.IsSuccess = true;
                dbResponse.RowId = _GetPrimaryKeyValue(DbContext, entity);

                return dbResponse;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        dbResponse.Message += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                return dbResponse;
            }
        }

        public RepoResponse Update(TEntity entity)
        {
            RepoResponse dbResponse = new RepoResponse() { IsSuccess = false };

            try
            {
                if (entity == null)
                {
                    dbResponse.Message = "NULL";

                    return dbResponse;
                }

                var dbSet = DbContext.Set<TEntity>();

                dbSet.Attach(entity);
                DbContext.Entry(entity).State = EntityState.Modified;
                DbContext.SaveChanges();

                dbResponse.IsSuccess = true;

                return dbResponse;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        dbResponse.Message += Environment.NewLine + string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

                return dbResponse;
            }
        }

        public RepoResponse Delete(TEntity entity)
        {
            RepoResponse dbResponse = new RepoResponse() { IsSuccess = false };

            try
            {
                if (entity == null)
                {
                    dbResponse.Message = "NULL";

                    return dbResponse;
                }

                var dbSet = DbContext.Set<TEntity>();

                if (DbContext.Entry(entity).State == EntityState.Detached)
                {
                    dbSet.Attach(entity);
                }

                dbSet.Remove(entity);
                DbContext.SaveChanges();

                dbResponse.IsSuccess = true;
            }
            catch (Exception ex)
            {
                dbResponse.Message = ex.ToString();
            }

            return dbResponse;
        }

        public TEntity FindByPk(long? primaryKey)
        {
            //var dbResult = DbContext.m_agent.Find(primaryKey);
            var dbSet = DbContext.Set<TEntity>();
            var result = dbSet.Find(primaryKey);

            return result;
        }

        public List<TEntity> Find(Expression<Func<TEntity, bool>> condition = null, string orderCol = null,
            int limit = 0, int offset = 0)
        {
            List<TEntity> listEntity = new List<TEntity>();

            var query = Enumerable.Empty<TEntity>().AsQueryable();

            try
            {
                var dbSet = DbContext.Set<TEntity>();

                if (condition != null)
                {
                    query = dbSet.Where(condition);
                }
                else
                {
                    query = dbSet.AsQueryable();
                }

                listEntity = query.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return listEntity;
        }

        public PaggingResult Paging(Expression<Func<TEntity, bool>> condition = null, string orderCol = null, int limit = 0, int offset = 0, bool isDesc = false)
        {
            PaggingResult result = new PaggingResult();

            var query = Enumerable.Empty<TEntity>().AsQueryable();

            try
            {
                var dbSet = DbContext.Set<TEntity>();

                if (condition != null)
                {
                    query = dbSet.Where(condition);
                }
                else
                {
                    query = dbSet.AsQueryable();
                }

                int totalRecords = query.Count();

                string orderFunc = IsDesc ? "OrderByDescending" : "OrderBy";
                //  string orderFunc = "OrderBy";

                query = _GetQueryFunction(query, orderCol, orderFunc);

                offset = ((offset - 1) * limit);

                if (limit > 0)
                {
                    query = query.Skip(offset).Take(limit);
                }

                double pageSize = (double)totalRecords / limit;
                result.PageSize = Math.Ceiling((pageSize));
                result.TotalRecord = totalRecords;
                result.Status = 1;
                result.ShowFrom = (offset + 1);

                if (totalRecords < ((offset + 1) + limit))
                {
                    result.ShowTo = totalRecords;
                }
                else
                {
                    result.ShowTo = ((offset) + limit);
                }
                result.Data = query.ToList();
            }
            catch (Exception ex)
            {
                result.Status = 0;
                throw new Exception(ex.ToString());
            }

            return result;
        }

        public PaggingResult PaggingBroadcast(Expression<Func<TEntity, bool>> condition = null, string orderCol = null, int limit = 0, int offset = 0, bool isDesc = false)
        {
            PaggingResult result = new PaggingResult();

            var query = Enumerable.Empty<TEntity>().AsQueryable();

            try
            {
                var dbSet = DbContext.Set<TEntity>();

                if (condition != null)
                {
                    query = dbSet.Where(condition);
                }
                else
                {
                    query = dbSet.AsQueryable();
                }

                int totalRecords = query.Count();

                //string orderFunc = IsDesc ? "OrderByDescending" : "OrderBy";
                string orderFunc = "OrderBy";

                query = _GetQueryFunction(query, orderCol, orderFunc);

                double pageSize = (double)totalRecords / limit;
                result.PageSize = Math.Ceiling((pageSize));
                result.TotalRecord = totalRecords;
                result.Status = 1;
                result.ShowFrom = (offset + 1);

                if (totalRecords < ((offset + 1) + limit))
                {
                    result.ShowTo = totalRecords;
                }
                else
                {
                    result.ShowTo = ((offset) + limit);
                }
                result.Data = query.ToList();
            }
            catch (Exception ex)
            {
                result.Status = 0;
                throw new Exception(ex.ToString());
            }

            return result;
        }

        private IOrderedQueryable<TEntity> _GetQueryFunction(IQueryable<TEntity> query, string colName, string funcName)
        {
            ParameterExpression[] typeParams = new ParameterExpression[] { Expression.Parameter(typeof(TEntity), string.Empty) };

            PropertyInfo pi = typeof(TEntity).GetProperty(colName);

            return (IOrderedQueryable<TEntity>)query.Provider.CreateQuery(
            Expression.Call(
             typeof(Queryable),
             funcName,
             new Type[] { typeof(TEntity), pi.PropertyType },
             query.Expression,
             Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
            );
        }

        public List<TEntity> NativeQuery(string query)
        {
            List<TEntity> listEntity = null;

            try
            {
                listEntity = new List<TEntity>();
                listEntity = DbContext.Database.SqlQuery<TEntity>(query).ToList();
            }
            catch (Exception exp)
            {
                exp.ToString();
                // do nothing
            }

            return listEntity;
        }
    }
}