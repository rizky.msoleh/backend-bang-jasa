﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Resource.Helper
{
    public class RepoResponse
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public long? RowId { get; set; }
    }

    public class PaggingResult
    {

        public int TotalRecord { get; set; }

        public int Offset { get; set; }

        public int Limit { get; set; }

        public double PageSize { get; set; }

        public object Data { get; set; }

        public int Status { get; set; }

        public int ShowFrom { get; set; }

        public int ShowTo { get; set; }

        public int location { get; set; }
        

    }
}