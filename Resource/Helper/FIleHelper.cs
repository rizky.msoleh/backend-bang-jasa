﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Resource.Helper
{
    public static class FileHelper
    {
        public static string UploadFileAPI(string fileType, string destinationFolder, HttpPostedFile file)
        {
            string DefaultDriveFolder = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["locationDriveUpload"] + ConfigurationManager.AppSettings["locationFolderUpload"]);
            if (!System.IO.Directory.Exists(DefaultDriveFolder))
            {
                System.IO.Directory.CreateDirectory(DefaultDriveFolder);
            }

            string DefaultFileFolder = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["locationDriveUpload"] + ConfigurationManager.AppSettings["locationFolderUpload"] + destinationFolder.Replace("\\","-"));
            if (!System.IO.Directory.Exists(DefaultFileFolder))
            {
                System.IO.Directory.CreateDirectory(DefaultFileFolder);
            }

            string ext = Path.GetExtension(file.FileName);
            string ImageName = DateTime.UtcNow.ToString("ddMMyyHHmmss");
            ImageName = fileType + ImageName + ext;
            var pathOriginal = Path.Combine(DefaultFileFolder, ImageName);
            file.SaveAs(pathOriginal);

            string ImageFolderReturn = Path.Combine(ConfigurationManager.AppSettings["locationDriveUpload"] + "/" + ConfigurationManager.AppSettings["locationFolderUpload"] + "/" + destinationFolder, ImageName);
            return ImageFolderReturn.Replace("\\", "/").Replace("//", "/").Replace("~/", "");

        }

        public static void DeleteFile(string pathFile)
        {
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("~\\" + pathFile.Replace("\\", "/").Replace("//", "/"))))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath("~\\" + pathFile.Replace("\\", "/").Replace("//", "/")));
            }
        }

        public static string GetFileUrl(this string path)
        {
            string baseUrl = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + System.Configuration.ConfigurationManager.AppSettings.Get("Port") +
                             HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/" + path;
            return baseUrl;
        }
    }
}