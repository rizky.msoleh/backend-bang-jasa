﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Resource.Helper
{
    public static class DateTimeHelper
    {
        public static DateTime StringToDateTime(this string strDate)
        {
          return  DateTime.ParseExact(strDate, "yyyy-MM-dd",
                System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string DateTimeToString(this DateTime date)
        {
            return date.ToString("yyyy-MM-DD");
        }


        public static string DateTimeNullAbleToString(this DateTime? date)
        {
            if (date != null)
            {
                return date.Value.ToString("yyyy-MM-DD");
            }
            else
            {
                return "";
            }
        }
    }
}