﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using Resource.Models;

namespace Resource.Helper
{
    public class JsonHelper
    {
        public static HttpResponseMessage JsonResponse(JsonEntity json, System.Net.Http.HttpRequestMessage request)
        {
            string stringJson = JsonConvert.SerializeObject(json, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
            var response = request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(stringJson, Encoding.UTF8, "application/json");

            try
            {
                // write to log
                //LoggingHelper.AppLogging(request.RequestUri.ToString(), stringJson);
            }
            catch (Exception e)
            {
                // do nothing
            }

            return response;
        }
    }

    public class JsonLowerCaseUnderscoreContractResolver : DefaultContractResolver
    {
        private Regex regex = new Regex("(?!(^[A-Z]))([A-Z])");

        protected override string ResolvePropertyName(string propertyName)
        {
            return regex.Replace(propertyName, "_$2").ToLower();
        }
    }
}
