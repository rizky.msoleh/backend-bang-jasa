﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class KondisiKorbanVM
    {
        public KondisiKorbanVM()
        {
        }

        public KondisiKorbanVM(KondisiKorban entity)
        {
            IdKondisiKorban = entity.IdKondisiKorban;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdKondisiKorban { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}