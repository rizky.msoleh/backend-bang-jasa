﻿using System;
using System.Web;
using Resource.Helper;
using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class PengajuanKecelakaanVM
    {
        public PengajuanKecelakaanVM(PengajuanKecelakaan entity)
        {
            IdPengajuan = entity.IdPengajuan;
            NomorPelaporan = entity.NomorPelaporan;
            NIK = entity.NIK;
            Nama = entity.Nama;
            TanggalKecelakaan = entity.TanggalKecelakaan.DateTimeToString();
            NamaProvinsi = entity.Provinsi.Nama;
            NamaKabupaten = entity.Kabupaten.Nama;
            NamaKota = entity.Kota.Nama;
            Umur = entity.Umur;
            JenisKelamin = entity.JenisKelamin;
            Alamat = entity.Alamat;
            NamaKondisiKorban = entity.KondisiKorban.Nama;
            NamaKasusKecelakaan = entity.KasusKecelakaan.Nama;
            NamaHubunganPelapor = entity.HubunganPelapor.Nama;
            NamaPelapor = entity.Nama;
            HandphonePelapor = entity.HandphonePelapor;
            NamaProvinsiPelapor = entity.Provinsi.Nama;
            NamaKotaPelapor = entity.Kota.Nama;
            NamaFaskes = entity.Faske.Nama;
            KTP = (!string.IsNullOrEmpty(entity.KTP)) ? entity.KTP.GetFileUrl() : "";
            KwitansiRS = (!string.IsNullOrEmpty(entity.KwitansiRS)) ? entity.KwitansiRS.GetFileUrl() : ""; 
            SuratKeteranganKesehatan = (!string.IsNullOrEmpty(entity.SuratKeteranganKesehatan)) ? entity.SuratKeteranganKesehatan.GetFileUrl() : ""; 
            SuratKeteranganAhliWaris = (!string.IsNullOrEmpty(entity.SuratKeteranganAhliWaris)) ? entity.SuratKeteranganAhliWaris.GetFileUrl() : ""; 
            LaporanPolisi = entity.LaporanPolisi;
            NoLaporan = entity.NoLaporan;
            SuratLaporanPolisi = (!string.IsNullOrEmpty(entity.SuratLaporanPolisi)) ? entity.SuratLaporanPolisi.GetFileUrl() : ""; 
        }

        public long IdPengajuan { get; set; }
        public string NomorPelaporan { get; set; }
        public string NIK { get; set; }
        public string Nama { get; set; }
        public string TanggalKecelakaan { get; set; }
        public string NamaProvinsi { get; set; }
        public string NamaKabupaten { get; set; }
        public string NamaKota { get; set; }
        public int Umur { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string NamaKondisiKorban { get; set; }
        public string NamaKasusKecelakaan { get; set; }
        public string NamaHubunganPelapor { get; set; }
        public string NamaPelapor { get; set; }
        public string HandphonePelapor { get; set; }
        public string NamaProvinsiPelapor { get; set; }
        public string NamaKotaPelapor { get; set; }
        public string NamaFaskes { get; set; }
        public string KTP { get; set; }
        public string KwitansiRS { get; set; }
        public string SuratKeteranganKesehatan { get; set; }
        public string SuratKeteranganAhliWaris { get; set; }
        public bool LaporanPolisi { get; set; }
        public string NoLaporan { get; set; }
        public string SuratLaporanPolisi { get; set; }
    }

    public class PengajuanKecelakaanUpdateVM : PengajuanKecelakaanCreateVM
    {
        public long IdPengajuan { get; set; }
    }

    public class PengajuanKecelakaanCreateVM
    {      
        public string NIK { get; set; }
        public string Nama { get; set; }
        public string TanggalKecelakaan { get; set; }
        public long IdProvinsi { get; set; }
        public long IdKabupaten { get; set; }
        public long IdKota { get; set; }
        public int Umur { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public long IdKondisiKorban { get; set; }
        public long IdKasusKecelakaan { get; set; }
        public long IdHubunganPelapor { get; set; }
        public string NamaPelapor { get; set; }
        public string HandphonePelapor { get; set; }
        public long IdProvinsiPelapor { get; set; }
        public long IdKotaPelapor { get; set; }
        public long IdFaskes { get; set; }
        public HttpPostedFile KTP { get; set; }
        public HttpPostedFile KwitansiRS { get; set; }
        public HttpPostedFile SuratKeteranganKesehatan { get; set; }
        public HttpPostedFile SuratKeteranganAhliWaris { get; set; }
        public bool LaporanPolisi { get; set; }
        public string NoLaporan { get; set; }
        public HttpPostedFile SuratLaporanPolisi { get; set; }
    }
}