﻿using Resource.Helper;
using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class PembayaranVM
    {
        public PembayaranVM()
        { }


        public PembayaranVM(Pembayaran entity)
        {
            IdPembayaran = entity.IdPembayaran;
            CustomerId = entity.CustomerId;
            NomorPembayaran = entity.NomorPembayaran;
            Keterangan = entity.Keterangan;
            Jumlah = entity.Jumlah;
            TanggalJatuhTempo = entity.TanggalJatuhTempo.DateTimeToString();
            UrlPembayaran = entity.UrlPembayaran;
            TanggalPembayaran = entity.TanggalPembayaran.DateTimeNullAbleToString();
            Status = entity.Status;
        }

        public long IdPembayaran { get; set; }
        public long CustomerId { get; set; }
        public string NomorPembayaran { get; set; }
        public string Keterangan { get; set; }
        public long Jumlah { get; set; }
        public string TanggalJatuhTempo { get; set; }
        public string UrlPembayaran { get; set; }
        public string TanggalPembayaran { get; set; }
        public int Status { get; set; }
    }



}