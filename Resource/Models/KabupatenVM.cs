﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class KabupatenVM
    {
        public KabupatenVM()
        {
        }

        public KabupatenVM(Kabupaten entity)
        {
            IdKabupaten = entity.IdKabupaten;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdKabupaten { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}