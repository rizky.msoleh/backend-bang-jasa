﻿using Newtonsoft.Json;
using Resource.Models.DatabaseModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Resource.Helper;

namespace Resource.Models
{
    public class CustomerVM
    {
        public CustomerVM(Customer entity)
        {
            CustomerId = entity.CustomerId;
            Username = entity.Username;
            Username = entity.Username;
            Handphone = entity.Handphone;
            Email = entity.Email;
            Foto = (!string.IsNullOrEmpty(entity.Foto)) ? entity.Foto.GetFileUrl() : ""; ;
        }

        public long CustomerId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Handphone { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Foto { get; set; }
    }

    public class CustomerLoginVM
    {
        [Required(ErrorMessage = "Username is Required")]
        [JsonProperty(PropertyName = "username")]
        [StringLength(20, ErrorMessage = "Username must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string username { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        [JsonProperty(PropertyName = "password")]
        [StringLength(int.MaxValue, ErrorMessage = "Password must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string password { get; set; }
    }

    public class RegisterVM
    {
        [Required(ErrorMessage = "Username is Required")]
        [JsonProperty(PropertyName = "username")]
        [StringLength(20, ErrorMessage = "Username must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string username { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        [JsonProperty(PropertyName = "password")]
        [StringLength(int.MaxValue, ErrorMessage = "Password must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string password { get; set; }

        [Required(ErrorMessage = "Fullname is Required")]
        public string fullname { get; set; }

        [Required(ErrorMessage = "Handphone is Required")]
        public string handphone { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        public string email { get; set; }
    }


    public class CustomerUpdateVM
    {

        public long CustomerId { get; set; }

        [Required(ErrorMessage = "Username is Required")]
        [JsonProperty(PropertyName = "username")]
        [StringLength(20, ErrorMessage = "Username must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string username { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        [JsonProperty(PropertyName = "password")]
        [StringLength(int.MaxValue, ErrorMessage = "Password must be at least {2} characters long And Maximun {1} characters long", MinimumLength = 6)]
        public string password { get; set; }

        public string handphone { get; set; }

        public HttpPostedFile photo { get; set; }

        [Required(ErrorMessage = "Fullname is Required")]
        public string fullname { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        public string email { get; set; }
    }
}