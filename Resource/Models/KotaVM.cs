﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class KotaVM
    {
        public KotaVM()
        {
        }

        public KotaVM(Kota entity)
        {
            IdKota = entity.IdKota;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdKota { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}