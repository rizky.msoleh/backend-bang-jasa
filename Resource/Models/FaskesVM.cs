﻿using Resource.Models;
using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class FaskesVM
    {
        public FaskesVM()
        {
        }

        public FaskesVM(Faske entity)
        {
            IdFaskes = entity.IdFaskes;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdFaskes { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}