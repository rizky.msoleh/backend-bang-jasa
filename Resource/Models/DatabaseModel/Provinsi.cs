//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resource.Models.DatabaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Provinsi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Provinsi()
        {
            this.PengajuanKecelakaans = new HashSet<PengajuanKecelakaan>();
            this.PengajuanKecelakaans1 = new HashSet<PengajuanKecelakaan>();
        }
    
        public long IdProvinsi { get; set; }
        public string Nama { get; set; }
        public bool Dihapus { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PengajuanKecelakaan> PengajuanKecelakaans { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PengajuanKecelakaan> PengajuanKecelakaans1 { get; set; }
    }
}
