﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class HubunganPelaporVM
    {
        public HubunganPelaporVM()
        {
        }

        public HubunganPelaporVM(HubunganPelapor entity)
        {
            IdHubunganPelapor = entity.IdHubunganPelapor;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdHubunganPelapor { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}