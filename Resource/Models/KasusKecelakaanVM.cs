﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class KasusKecelakaanVM
    {
        public KasusKecelakaanVM()
        {
        }

        public KasusKecelakaanVM(KasusKecelakaan entity)
        {
            IdKasusKecelakaan = entity.IdKasusKecelakaan;
            Terhapus = entity.Terhapus;
            Nama = entity.Nama;
        }

        public long IdKasusKecelakaan { get; set; }
        public string Nama { get; set; }
        public bool Terhapus { get; set; }
    }
}