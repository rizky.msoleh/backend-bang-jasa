﻿using System;

namespace Resource.Models
{
    public class AlertMessage
    {
        public int Status { get; set; }

        public string Text { get; set; }

        public object Data { get; set; }
    }

    public class Alert
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class JsonEntity
    {
        public bool error { get; set; }
        public Alert alerts { get; set; }
        public long total_data { get; set; }
        public Object data { get; set; }

        public JsonEntity()
        {
            this.alerts = new Alert();
            this.data = new Object();
        }

        public void SetError(bool error)
        {
            this.error = error;
        }

        public bool GetError()
        {
            return this.error;
        }

        public void AddAlert(string code, string message)
        {
            this.alerts.code = code;
            this.alerts.message = message;
        }

        public Alert GetAlert()
        {
            return this.alerts;
        }

        public void AddData(Object data)
        {
            this.data = data;
        }

        public void AddTotalData(long total)
        {
            this.total_data = total;
        }

        public Object GetData()
        {
            return this.data;
        }
    }
}