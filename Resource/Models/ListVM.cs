﻿namespace Resource.Models
{
    public class ListVM
    {
        public int? limit { get; set; }
        public int? offset { get; set; }
        public int? sort_code { get; set; }
    }
}