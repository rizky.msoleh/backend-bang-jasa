﻿using Resource.Models.DatabaseModel;

namespace Resource.Models
{
    public class ProvinsiVM
    {
        public ProvinsiVM() {}

        public ProvinsiVM(Provinsi entity)
        {
            IdProvinsi = entity.IdProvinsi;
            Dihapus = entity.Dihapus;
            Nama = entity.Nama;
        }

        public long IdProvinsi { get; set; }
        public string Nama { get; set; }
        public bool Dihapus { get; set; }
    }
}