﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BangJasa.Startup))]
namespace BangJasa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
